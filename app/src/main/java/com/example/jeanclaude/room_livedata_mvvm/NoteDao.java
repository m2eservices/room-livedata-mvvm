package com.example.jeanclaude.room_livedata_mvvm;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

// Grâce à Room, le code du DAO sera généré à partir des annotations

@Dao
public interface NoteDao {

    @Insert
    void insert(Note note);

    @Delete
    void delete(Note note);

    @Update
    void update(Note note);

    // Avec Room, on sait tout de suite à l'écriture si les requêtes fonctionnent ou on,
    // contrairement à avant avec SQLiteOpenHelper qui plantait si la requête était mal écrite
   @Query("DELETE FROM note_table")
    void deleteAllNotes();

   // On aurait pu simplement récupérer le résultat dans une liste, mais dans ce cas on aurait du
   // écrire tout le code pour les mises à jour de cette liste en cas de mise à jour des données dans
   // la BDD
   @Query("select * from note_table order by priority desc")
   LiveData<List<Note>> getAllNotes();
}
